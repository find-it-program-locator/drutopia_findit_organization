<!-- writeme -->
Drutopia Findit Organization
============================

A service providing organization that may put on programs and events or be stewards of places findable on this website.

 * https://gitlab.com/find-it-program-locator/drutopia_findit_organization<Paste>
 * Issues: https://gitlab.com/find-it-program-locator/drutopia_findit_organization/issues
 * Source code: https://gitlab.com/find-it-program-locator/drutopia_findit_organization/tree/8.x-1.x
 * Keywords: find it, drupal, drutopia, organization
 * Package name: drupal/drutopia_findit_organization


### Requirements

 * drupal/drutopia_core ^1.0-alpha1
 * drupal/entity_reference_validators ^1.0@alpha


### License

GPL-2.0+

<!-- endwriteme -->
