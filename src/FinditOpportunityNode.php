<?php

namespace Drupal\drutopia_findit_organization;

use DateTime;
use DateTimeZone;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;
use Drupal\profile\Entity\Profile;

/**
 * Provides a subclass for Find It opportunity bundles of the node entity type.
 *
 * @package Drupal\drutopia_findit_organization\Entity\Node
 */
class FinditOpportunityNode extends Node {

  public static $default_relative_time = '-37 months';
  public static $default_display_time = 'three years';

  public static function getOutdatedTypeTimes() {
    return [
      'findit_event' => [
        'relative_time' => '-13 months',
        'display_time' => t('a year'),
      ],
      'findit_program' => [
        'relative_time' => '-13 months',
        'display_time' => t('a year'),
      ],
      'findit_organization' => [
        'relative_time' => '-25 months',
        'display_time' => t('two years'),
      ],
    ];
  }

  /**
   * Return relative time or human readable equivalent.
   *
   * @param bool $display
   *   Whether to return the display time or the relative time.
   *
   * @return string
   */
  public function getOutdatedTime($display = FALSE) {
    $outdated_times = self::getOutdatedTypeTimes();
    $bundle = $this->getType();
    // Default; used for $bundle === 'findit_place'
    $relative_time = self::$default_relative_time;
    $display_time = self::$default_display_time;
    if (isset($outdated_times[$bundle])) {
      $relative_time = $outdated_times[$bundle]['relative_time'];
      $display_time = $outdated_times[$bundle]['display_time'];
    }
    return ($display) ? $display_time : $relative_time;
  }

  /**
   * Print 'a year', 'two years' etc. for time since update considered stale.
   *
   * This is used from the node-base--opportunity--full.html.twig template.
   *
   * @return string
   */
  public function getOutdatedPeriod() {
    return $this->getOutdatedTime(TRUE);
  }

  /**
   * Return TRUE if an opportunity is outdated (hasn't been updated for a while)
   */
  public function isOutdated() {
    $updated = $this->getChangedTime();

    $relative_time = $this->getOutdatedTime();
    $oldest_allowable_update = $this::getUnixDate($relative_time);

    return $updated < $oldest_allowable_update;
  }

  /**
   * Returns TRUE if an opportunity's registration has closed (only applies to events and programs).
   */
  public function isRegistrationEnded() {
    // If the registration 'when' setting is ongoing or otherwise not specific dates
    $type = $this->getField('field_findit_registration_type');
    if ($type === 'none' || $type === 'optional') {
      return FALSE;
    }
    if ($this->getField('field_findit_registration_when') !== 'specific_dates') {
      return FALSE;
    }
    $registration_dates = $this->getField('field_findit_register_dates');
    foreach ($registration_dates as $key => $date) {
      // @TODO do the right timezone, for now err on *not* saying it expired.
      if ($date['end_value'] >= $this::getUnixDate('-6 hours')) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Return a normalized, East Coast time zone Unix-formatted date.
   */
  public static function getUnixDate($relative_time = NULL) {
    // @TODO stop presuming East Coast times.
    $date = new DateTime($relative_time, new DateTimeZone('America/New_York'));
    $utc_date = new DateTime($date->format('@U'), new DateTimeZone('UTC'));
    return $utc_date->format('U');
  }

  /**
   * Return a normalized UTC ISO-formatted date.
   */
  public static function getIsoDate($timestamp) {
    // True ISO format would be from this:
    // $date = date_create("@$timestamp");
    // return $date->format('c');
    return gmDate("Y-m-d\TH:i:s\Z", $timestamp); // This attempt to correct actually does the opposite: + date("Z"));
  }

  /**
   * This function return the nearest date of the current day.
   *
   * Also ensures order, and optional filter to dates.
   *
   * @param array $date_field
   *
   * @return string[]
   */
  public function getDates($relative_from = 'now', $relative_to = NULL) : array {
    $field_name = 'field_findit_opportunity_dates';
    $opportunity_dates = [];
    if (!$this->hasField($field_name)) {
      return $opportunity_dates;
    }
    $raw_dates = $this->get($field_name)->getValue();
    // @TODO this way of bringing in from and to dates *only* allows relative dates.
    // If you feed strtotime a timestamp it gives you stuff in the year 5000.
    $time_from_date = strtotime($relative_from);
    if ($relative_to) {
      $time_to_date = strtotime($relative_to);
    }
    // This is what Gnuget did so holding out possibility required for
    // compatibility with City of Cambridge sitecore import.
    if (empty($raw_dates)) {
      return ['start_time' => '', 'end_time' => ''];
    }
    $difference = [];
    foreach ($raw_dates as $item) {
      if ($item['value'] >= $time_from_date && (!$relative_to || $item['value'] <= $time_to_date)) {
        $difference[$item['value']] = $item['value'] - $time_from_date;
        $dates[$item['value']] = [
          'start_time' => $this::getIsoDate($item['value']),
          'end_time' => $this::getIsoDate($item['end_value']),
        ];
      }
    }
    asort($difference);
    $nearest = array_keys($difference);
    foreach ($nearest as $val) {
      $opportunity_dates[] = $dates[$val];
    }
    return $opportunity_dates;
  }

  /**
   * Returns the value of a field if the field exists, null otherwise.
   */
  public function getField(string $field_name) {
    // If the content type we're on doesn't have this field, bail right away.
    if (!$this->hasField($field_name)) {
      return NULL;
    }
    $field = $this->get($field_name);
    if ($field->getFieldDefinition()->getFieldStorageDefinition()->getCardinality() === 1) {
      return $field->value;
    }
    else {
      return $field->getValue();
    }
  }

  /**
   * Format the the changed time as a date.
   */
  public function getChangedDate() {
    $updated = $this->getChangedTime();
    $timezone = date_default_timezone_get();
    $date = DrupalDateTime::createFromTimestamp($updated, timezone_open($timezone));
    return $date->format('F j, Y');
  }

  /**
   * Fetch and prepare solr score and boost information, if available.
   *
   * Hackily added at find-it-program-locator/findit:d5b58ddc3e345f8097c9fd3b8e91543c456f7751
   */
  public function getSolrInfo() {
    if (!\Drupal::currentUser()->hasPermission('administer search_api')) {
      return "";
    }
    $solr_info = $score = $boost = "";
    if (isset($this->solr_score)) {
      $solr_info = TRUE;
      $score = $this->solr_score;
    }
    if (isset($this->solr_boost)) {
      $solr_info = TRUE;
      $boost = $this->solr_boost;
    }
    if (!$solr_info) {
      return $solr_info;
    }
    $solr_info = "<pre >    score: $score
    boost: $boost</pre>";
    return $solr_info;
  }

  /**
   * Provides a button to e-mail service providers to ask for help.
   *
   * The button only shows on applicable opportunities, so returns an empty
   * string rather than HTML of a mailto link styled as a button when not a
   * program, event, or place opportunity with a contact that has an e-mail
   * address.
   */
  public function getRequestAssistanceButton() {
    // For now only Program, Event, and Place opportunities offer inquiries.
    if ('findit_organization' === $this->getType()) {
      return '';
    }

    $button = '';

    $contact_email = $this->getOneContactEmail();
    // Presently we don't show the button if there's no public address to
    // contact, but may switch to e-mailing just Find It itself in this case.
    if (!$contact_email) {
      return;
    }

    $mail_config = \Drupal::config('findit_mail_events.mail');
    $microformats_settings = \Drupal::config('microformats.settings');
    if ($microformats_settings && $siteinfo_email = $microformats_settings->get('siteinfo_email')) {
      $cc_findit_address = $siteinfo_email;
    }
    else {
      $cc_findit_address = \Drupal::config('system.site')->get('mail');
    }
    // @TODO look up if we should pass in current user for this, probably not,
    // but maybe the node author if we switch to making a webform here.
    $token_data = [
      'node' => $this,
    ];
    $subject = \Drupal::token()->replace($mail_config->get('request_provider_assistance.subject'), $token_data);
    $body = \Drupal::token()->replace($mail_config->get('request_provider_assistance.body'), $token_data);
    $cc = '&cc=' . $cc_findit_address;
    $from = \Drupal::currentUser()->getEmail();
    if ($from) {
      $cc .= '&cc=' . $from;
    }
    // We use rawurlencode, not urlencode, so we don't end up with plus signs for spaces.
    $href = 'mailto:?to=' . $contact_email . '&subject=' . rawurlencode($subject) . '&body=' . rawurlencode($body) . $cc;

    // @TODO use a template for this.
    $button = '<a href="' . $href . '" class="button drum is-hidden-print">' . t('Request assistance') . '</a>';
    return [
      '#type' => 'markup',
      '#markup' => $button,
    ];
  }

  /**
   * Return the first e-mail for a contact on this opportunity we find.
   *
   * Returns NULL if none available.
   */
  public function getOneContactEmail() {
    $contacts = $this->get('field_findit_contacts');
    foreach ($contacts as $contact) {
      if ($val = $contact->getValue()) {
        // @TODO put all this in a helper function in Contacts module.
        $contact_id = $val['target_id'];
        $contact_profile = Profile::load($contact_id);
        if ($contact_profile && isset($contact_profile->field_public_email)) {
          return $contact_profile->field_public_email->value;
        }
      }
    }
    return NULL;
  }

}
