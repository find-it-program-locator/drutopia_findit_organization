<?php

namespace Drupal\drutopia_findit_organization\Plugin\ExtraField\Display;

use DateTime;
use DateTimeZone;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Drupal\node\NodeInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\Item;
use Drupal\search_api\Query\Query;

/**
 * Example Extra field Display.
 *
 * @ExtraFieldDisplay(
 *   id = "child_opportunities",
 *   label = @Translation("Programs and past programs, upcoming events and past events, belonging to an organization."),
 *   bundles = {
 *     "node.findit_organization",
 *   }
 * )
 */
class ChildOpportunities extends ExtraFieldDisplayBase {

  const LIMIT = 50;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $events_query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $upcoming_query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $past_events_query;

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {

    $timezone = \Drupal::config('system.date')->get('timezone.default');
    $today_start = new DateTime('today', new DateTimeZone($timezone));
    $today_end =  new DateTime('tomorrow', new DateTimeZone($timezone));

    // We build queries with the minimum of repetition, cloning to use the same
    // query without interference, but with a different 'types' condition etc.

    $index_id = 'main';  // See parameter name 'drutopia_findit_search.index_id' in services.
    $this->query = new Query(Index::load($index_id));
    $this->query->addCondition('organizations', $entity->id(), 'IN');
    $this->query->range(0, self::LIMIT);
    $this->query->addCondition('status', NodeInterface::PUBLISHED);

    // Upcoming

    $this->upcoming_query = clone $this->query;
    $conditionGroup = $this->upcoming_query->createConditionGroup('OR', ['nonobsolete']);
    $conditionGroup->addCondition('dates_end', $today_end->format(DATE_ISO8601), '>');
    $conditionGroup->addCondition('dates_end', NULL);
    $this->upcoming_query->addConditionGroup($conditionGroup);

    // Events (upcoming)

    $this->events_query = clone $this->upcoming_query;
    $this->events_query->addCondition('types', 'findit_event', 'IN');
    $this->events_query->execute();
    $result = $this->events_query->getResults();
    $events = $this::prepareOpportunities($result);

    // Programs (upcoming)

    $this->upcoming_query->addCondition('types', 'findit_program', 'IN');
    $this->upcoming_query->execute();
    $result = $this->upcoming_query->getResults();
    $programs = $this::prepareOpportunities($result);


    // Past - we can start to take over our original query now

    $this->query->addCondition('dates_end', $today_end->format(DATE_ISO8601), '<');

    // Events (past)
    $this->past_events_query = clone $this->query;
    $this->past_events_query->addCondition('types', 'findit_event', 'IN');
    $this->past_events_query->execute();
    $result = $this->past_events_query->getResults();
    $past_events = $this::prepareOpportunities($result);


    // Programs (past)

    // We can now continue to use the original query for programs.
    $this->query->addCondition('types', 'findit_program', 'IN');
    $this->query->execute();
    $result = $this->query->getResults();
    $past_programs = $this::prepareOpportunities($result);

    // @TODO Link to a set of results filtered to those offered by an organization
    // Note that $result->getResultCount() gives us the total count, not what our range limits the results to, so we could use
    // if ( $result->getResultCount() > self::LIMIT ) to make the optional link.

    return [
      '#theme' => 'child_opportunities',
      '#programs' =>$programs,
      '#events' => $events,
      '#past_programs' => $past_programs,
      '#past_events' => $past_events,
    ];
  }

  static function prepareOpportunities($result) {
    if ($result->getResultCount()) {
      $map = function (Item $item) {
        return $item->getOriginalObject()->getValue();
      };
      $raw_opportunities = \Drupal::entityTypeManager()->getViewBuilder('node')->viewMultiple(array_map($map, $result->getResultItems()), 'teaser');
      // NOT a hack i like, but it's the only way i can get these nodes to not just render as empty shells
      // (really! it printed the template without any of the text).  This seems to be related to prerender:
      // https://www.drupal.org/project/drupal/issues/2099131
      // https://www.drupal.org/project/drupal/issues/2151459
      render($raw_opportunities);
      return array_filter($raw_opportunities, function ($key) { return substr($key, 0, 1) !== '#'; }, ARRAY_FILTER_USE_KEY);
    }
    else {
      return '';
    }
  }
}

