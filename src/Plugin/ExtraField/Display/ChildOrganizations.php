<?php

namespace Drupal\drutopia_findit_organization\Plugin\ExtraField\Display;

use DateTime;
use DateTimeZone;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Drupal\node\NodeInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\Item;
use Drupal\search_api\Query\Query;

/**
 * Example Extra field Display.
 *
 * @ExtraFieldDisplay(
 *   id = "child_organizations",
 *   label = @Translation("Child organizations, organizations which are affiliated with an organization (identify it as a parent organization."),
 *   bundles = {
 *     "node.findit_organization",
 *   }
 * )
 */
class ChildOrganizations extends ExtraFieldDisplayBase {

  const LIMIT = 50;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $events_query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $upcoming_query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $past_events_query;

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {

    $index_id = 'main';  // See parameter name 'drutopia_findit_search.index_id' in services.
    $this->query = new Query(Index::load($index_id));
    $this->query->addCondition('organizations', $entity->id(), 'IN');
    $this->query->range(0, self::LIMIT);
    $this->query->addCondition('status', NodeInterface::PUBLISHED);
    $this->query->addCondition('types', 'findit_organization', 'IN');
    $this->query->execute();
    $result = $this->query->getResults();
    $organizations = $this::prepareOpportunities($result);

    return [
      '#theme' => 'child_organizations',
      '#organizations' => $organizations,
    ];
  }

  static function prepareOpportunities($result) {
    if ($result->getResultCount()) {
      $map = function (Item $item) {
        return $item->getOriginalObject()->getValue();
      };
      $raw_opportunities = \Drupal::entityTypeManager()->getViewBuilder('node')->viewMultiple(array_map($map, $result->getResultItems()), 'teaser');
      // NOT a hack i like, but it's the only way i can get these nodes to not just render as empty shells
      // (really! it printed the template without any of the text).  This seems to be related to prerender:
      // https://www.drupal.org/project/drupal/issues/2099131
      // https://www.drupal.org/project/drupal/issues/2151459
      render($raw_opportunities);
      return array_filter($raw_opportunities, function ($key) { return substr($key, 0, 1) !== '#'; }, ARRAY_FILTER_USE_KEY);
    }
    else {
      return '';
    }
  }
}

