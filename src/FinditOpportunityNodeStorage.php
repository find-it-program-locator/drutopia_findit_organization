<?php

namespace Drupal\drutopia_findit_organization;

use Drupal\drutopia_findit_organization\FinditOpportunityNode;
use Drupal\node\NodeStorage;

/**
 * Provides a subclass for Find It opportunity bundles of the node entity type.
 *
 * @package Drupal\drutopia_findit_organization\Entity\Node
 */
class FinditOpportunityNodeStorage extends NodeStorage {

  /**
   * {@inheritDoc}
   */
  public static function bundleClasses() {
    $bundle_classes = parent::bundleClasses();
    // Right now we're all using the same class but likely we'll do a class for
    // each opportunity bundle that itself extends the node-extending class.
    $bundle_classes['findit_organization'] = FinditOpportunityNode::class;
    $bundle_classes['findit_program'] = FinditOpportunityNode::class;
    $bundle_classes['findit_event'] = FinditOpportunityNode::class;
    $bundle_classes['findit_place'] = FinditOpportunityNode::class;
    return $bundle_classes;
  }

}
